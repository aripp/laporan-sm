<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\PoliController;
use App\Http\Controllers\Web\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::get('/poli/demografi', [PoliController::class, 'demografi'])->name('poli.demografi');

    
    Route::get('/user', [UserController::class, 'index'])->name('user.index');
    Route::get('/user/get-listing', [UserController::class, 'listing'])->name('user.listing');
    Route::get('/user/search-karyawan', [UserController::class, 'searchKaryawan'])->name('user.search-karyawan');
    Route::get('/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user/store', [UserController::class, 'store'])->name('user.store');
    Route::get('/user/{id}/edit', [UserController::class, 'edit'])->name('user.edit');
    Route::get('/user/{id}/delete', [UserController::class, 'delete'])->name('user.delete');
    Route::post('/user/{id}/update', [UserController::class, 'update'])->name('user.update');
});



Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate'])->name('auth.check');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
