<footer class="main-footer">
    Copyright &copy; 2022 - <?=date('Y')?> RSU Islam Boyolali.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>