<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>@yield('title', 'Laporan')</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/assets/adminlte/plugins/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="/assets/adminlte/css/adminlte.min.css">
  <link rel="stylesheet" href="/assets/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">


  <script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
  <script src="/assets/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="/assets/adminlte/js/adminlte.js"></script>
  <script src="/assets/js/app.js"></script>
  @yield('plugin')

</head>
<body class="sidebar-mini control-sidebar-slide-open layout-navbar-fixed text-sm">

  <div class="wrapper">

    @include('layout.preloader')

    @include('layout.navbar')

    @include('layout.sidebar')


    <div class="content-wrapper">
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h4 class="m-0">@yield('breadcumb')</h4>
            </div>
          </div>
        </div>
      </section>

      <section class="content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </section>

    </div>

    @include('layout.footer')
    <div id="loading" class="overlay-wrapper">
      <div class="overlay d-flex flex-column">
        <i class="fas fa-3x fa-sync-alt fa-spin"></i>
        <div class="text-bold pt-2">Loading...</div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    window.isLoading = (status) => {
      if(status){
        $('#loading').removeClass('d-none')
      }else{
        $('#loading').addClass('d-none')
      }

      return true;
    }
    window.isLoading(false)
  </script>
  @yield('javascript')

</body>
</html>
