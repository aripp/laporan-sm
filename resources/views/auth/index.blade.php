
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laporan-RM | Login</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="/assets/adminlte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/assets/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="/assets/adminlte/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">

  <div class="card card-outline card-success">
    <div class="card-header text-center">
      <h2><b>Laporan</b> RM</h2>
    </div>
    <div class="card-body">
      @if(session('status'))
      <div class="alert alert-danger">
        {{ session('status') }}
      </div>
      @endif
      <form action={{ route('auth.check') }} method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" name="username" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          @error('username')
            <div class="invalid-feedback"> {{ $message }} </div>
          @enderror
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
            <div class="invalid-feedback"> {{ $message }} </div>
          @enderror
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-success">
              <input type="checkbox" name="remember" id="remember" value="true">
              <label for="remember">
                Ingat saya
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-4 offset-8">
            <button type="submit" class="btn btn-success btn-block">Sign In</button>
          </div>
        
        </div>
      </form>
    </div>
  
  </div>
</div>
<script src="/assets/adminlte/plugins/jquery/jquery.min.js"></script>
<script src="/assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/adminlte/js/adminlte.min.js"></script>
</body>
</html>
