@extends('layout.app')
@section('title', 'Poli - Demografi')
@section('breadcumb', 'Demografi')

@section('content')
    <div class="row">

      <div class="col-md-8">
        <div class="card card-success card-outline">
          <div class="card-header">
            <h3 class="card-title">Filter</h3>
          </div>
          <div class="card-body" >
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="">Tanggal</label>
                <input type="text" id="tanggal" class="form-control" placeholder="Tanggal" value="" autocomplete="off">
              </div>
              <div class="form-group col-md-8">
                <label for="">Poli</label>
                <select class="form-control select2" style="width: 100%;" id="poli">
                  <option selected="selected"></option>
                </select>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="row justify-content-center">
              <div class="col-sm-3">
                <button class="btn btn-success btn-sm btn-block" id="btn-tampilkan">Tampilkan</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-info card-outline">
          <div class="card-header">
            <h3 class="card-title">Download raw data</h3>
          </div>
          <div class="card-body" >
            <p> Total data : <span id="total_data" class="font-weight-bold"> 0 </span></p>
            <div class="row justify-content-center">
              <div class="col-md-12">
                <button class="btn btn-info btn-sm btn-block" id="btn-tampilkan">Download</button>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div class="col-md-3">
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title">Jenis Kelamin</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="canvasJkWrapper"></div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title">Agama</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="canvasAgamaWrapper" ></div>
          </div>
        </div>
        <div class="col-md-5">
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title">Pendidikan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="canvasPendidikanWrapper" ></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title">Jenis Kunjungan</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="canvasKunjunganWrapper" ></div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card collapsed-card">
            <div class="card-header">
              <h3 class="card-title">Asuransi</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body" id="canvasAsuransiWrapper"></div>
          </div>
        </div>
      

    </div>

@endsection

@section('javascript')
<script type="text/javascript">
  (function ($) {
    $(document).ready(function () {
      main()
    })

    const main = () => {
      setTanggal();
      setPoliSelect2()

      $('#btn-tampilkan').click( e => {
        let tanggal = $('#tanggal').val()
        let arrTanggal = tanggal.split(" sd ")
        let tanggalMulai = arrTanggal[0]
        let tanggalSelesai = arrTanggal[1]
        let poli = $('#poli').val()
        if(!tanggalMulai || !tanggalSelesai || !poli){
          toastr.error('tanggal dan poli wajib di isi')
          return false;
        }
        getLaporan(tanggalMulai, tanggalSelesai, poli)
      })
    }
    const setTanggal = () => {
      $('#tanggal').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
            format: "DD-MM-YYYY",
            separator: " sd ",
        }
      });
      $('#tanggal').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' sd ' + picker.endDate.format('DD-MM-YYYY'));
      });
      $('#tanggal').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });
  

      return true;
    }
    const setPoliSelect2 = () => {
      $.get(window.origin + '/api/poli/demografi/get-poli').done(function(response){
        const data = response.data.map( e => ({ id: e.poli_kd, text: e.poli_nama}))
        $('#poli').select2({
            placeholder: 'Pilih poli',
            allowClear: true,
            theme: 'bootstrap4',
            data: [{id: 'SEMUA', text: "SEMUA POLI"}, ...data]
          });
      });

    }
    const getLaporan = ( start_date, end_date, poli ) => {
      window.isLoading(true);
      
      $.post(window.origin + '/api/poli/demografi/get-laporan', {start_date, end_date, poli})
      .done(function(response){
        $(".collapsed-card").removeClass('collapsed-card')
        
        chartJk(response.data.jenis_kelamin)
        chartKunjungan(response.data.kunjungan)
        chartAsuransi(response.data.asuransi)
        chartAgama(response.data.agama)
        chartPendidikan(response.data.pendidikan)

        $('#total_data').text(response.data.total_kujungan)


        console.log(response);
        window.isLoading(false);
      });
    }

    const chartJk = (data) =>{
      if(data.length < 1){
        toastr.error('grafik jk tidak dapat dibentuk, data kosong')
        return
      }
      
      let label = [];
      let value = [];
      let color = [];

      data.forEach( object =>{
        let arr = Object.entries(object)
        label.push(arr[0][1] || '-')
        value.push(arr[1][1])
        color.push(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`)
      })

      $('#chartJk').remove()
      $('#canvasJkWrapper').append('<canvas id="chartJk" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>')
      new Chart($('#chartJk').get(0).getContext('2d'), {
        type: 'pie',
        data: {
          labels: label,
          datasets: [{
                label: 'Jumlah',
                backgroundColor: color,
                borderColor: 'rgb(255, 255, 255)',
                data: value
            }]
        },
        options: {
                  maintainAspectRatio : false,
                  responsive : true,
                  plugins: {
                      datalabels: {
                          color: 'rgb(0, 0, 0)',
                          display: true,
                          align: 'center',
                          anchor: 'center'
                      }
                  }
                },
                
      })
    }
    const chartKunjungan = (data) =>{
      if(data.length < 1){
        toastr.error('grafik jenis kunjungan tidak dapat dibentuk, data kosong')
        return
      }
      
      let label = [];
      let value = [];
      let color = [];

      data.forEach( object =>{
        let arr = Object.entries(object)
        label.push(arr[0][1] || '-')
        value.push(arr[1][1])
        color.push(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`)
      })

      $('#chartKunjungan').remove()
      $('#canvasKunjunganWrapper').append('<canvas id="chartKunjungan" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>')
 
      new Chart( $('#chartKunjungan').get(0).getContext('2d') , {
        type: 'pie',
        data: {
          labels: label,
          datasets: [{
                label: 'Jumlah',
                backgroundColor: color,
                borderColor: 'rgb(255, 255, 255)',
                data: value
            }]
        },
        options: {
                  maintainAspectRatio : false,
                  responsive : true,
                  plugins: {
                      datalabels: {
                          color: 'rgb(0, 0, 0)',
                          display: true,
                          align: 'center',
                          anchor: 'center'
                      }
                  }
        },
                
      })
    }
    const chartAsuransi = (data) =>{
      if(data.length < 1){
        toastr.error('grafik asuransi tidak dapat dibentuk, data kosong')
        return
      }
      
      let label = [];
      let value = [];
      let color = [];

      data.forEach( object =>{
        let arr = Object.entries(object)
        label.push(arr[0][1] || '-')
        value.push(arr[1][1])
        color.push(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`)
      })

      $('#chartAsuransi').remove()
      $('#canvasAsuransiWrapper').append('<canvas id="chartAsuransi" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>')
      new Chart($('#chartAsuransi').get(0).getContext('2d'), {
        type: 'bar',
        data: {
            labels: label,
            datasets: [{
                label: 'Jumlah',
                backgroundColor: color,
                borderColor: 'rgb(255, 255, 255)',
                data: value
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            legend: {
                display: false
            },
            plugins: {
                datalabels: {
                    color: 'rgb(0, 0, 0)',
                    display: true,
                    align: 'center',
                    anchor: 'center'
                }
            }
        }
      });
    }
    const chartPendidikan = (data) =>{
      if(data.length < 1){
        toastr.error('grafik pendidikan tidak dapat dibentuk, data kosong')
        return
      }
      
      let label = [];
      let value = [];
      let color = [];

      data.forEach( object =>{
        let arr = Object.entries(object)
        label.push(arr[0][1] || '-')
        value.push(arr[1][1])
        color.push(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`)
      })

      $('#chartPendidikan').remove()
      $('#canvasPendidikanWrapper').append('<canvas id="chartPendidikan" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>')
      new Chart($('#chartPendidikan').get(0).getContext('2d'), {
        type: 'bar',
        data: {
            labels: label,
            datasets: [{
                label: 'Jumlah',
                backgroundColor: color,
                borderColor: 'rgb(255, 255, 255)',
                data: value
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            legend: {
                display: false
            },
            plugins: {
                datalabels: {
                    color: 'rgb(0, 0, 0)',
                    display: true,
                    align: 'center',
                    anchor: 'center'
                }
            }
        }
      });
    }
    const chartAgama = (data) =>{
      if(data.length < 1){
        toastr.error('grafik agama tidak dapat dibentuk, data kosong')
        return
      }
      
      let label = [];
      let value = [];
      let color = [];

      data.forEach( object =>{
        let arr = Object.entries(object)
        label.push(arr[0][1] || '-')
        value.push(arr[1][1])
        color.push(`rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`)
      })

      $('#chartAgama').remove()
      $('#canvasAgamaWrapper').append('<canvas id="chartAgama" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>')
      new Chart($('#chartAgama').get(0).getContext('2d'), {
        type: 'bar',
        data: {
            labels: label,
            datasets: [{
                label: 'Jumlah',
                backgroundColor: color,
                borderColor: 'rgb(255, 255, 255)',
                data: value
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            legend: {
                display: false
            },
            plugins: {
                datalabels: {
                    color: 'rgb(0, 0, 0)',
                    display: true,
                    align: 'center',
                    anchor: 'center'
                }
            }
        }
      });
    }

  })(jQuery)
</script>
@endsection

@section('plugin')
<link rel="stylesheet" href="/assets/adminlte/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/toastr/toastr.min.css">

<script src="/assets/adminlte/plugins/chart.js/Chart.min.js"></script>
<script src="/assets/adminlte/plugins/chart.js/Chart-datalabels.js"></script>
<script src="/assets/adminlte/plugins/moment/moment.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/assets/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="/assets/adminlte/plugins/select2/js/select2.full.min.js"></script>
<script src="/assets/adminlte/plugins/toastr/toastr.min.js"></script>

@endsection