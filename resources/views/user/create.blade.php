@extends('layout.app')
@section('title', "User - Create")
@section('breadcumb', "User")

@section('content')
{{-- {{ dd($errors)}} --}}
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Tambah user</h3>

            <div class="card-tools">
              <a href="{{ route('user.index') }}" class="btn btn-link">Kembali</a>
            </div>
          </div>
          <div class="card-body">
            <form action="{{ route("user.store") }}" method="POST" autocomplete="off">
              @csrf
              @if(session('status'))
              <div  class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
              <div class="form-row">
                <div class="form-group col-md-8">
                  <label>Karyawan</label>
                  <input type="hidden" name="nik_text" id="nik_text" value="{{ old('nik_text') }}">
                  <select class="form-control select2" style="width: 100%;" id="nik" name="nik">
                    <option value="{{ old('nik') }}" selected="selected">{{ old('nik_text') }}</option>
                  </select>
                  @error('nik')
                    <span class="text-danger"> {{ $message }} </span>
                  @enderror
                </div>
                <div class="form-group col-md-4">
                  <label>Username</label>
                  <input type="text" name="username" id="username" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" readonly>
                  @error('username')
                    <div class="invalid-feedback"> {{ $message }} </div>
                  @enderror
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-8">
                  <label>Akses</label>
                  <select class="form-control select2" style="width: 100%;" name="group">
                    <option selected="selected"></option>
                    <option value="ADMIN" @if(old('group') == 'ADMIN') selected @endif > ADMIN </option>
                    <option value="USER" @if(old('group') == 'USER') selected @endif >USER</option>
                  </select>
                  @error('group')
                    <span class="text-danger"> {{ $message }} </span>
                  @enderror
                </div>
                <div class="form-group col-md-4">
                  <label>Password</label>
                  <input type="text" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                  @error('password')
                    <div class="invalid-feedback"> {{ $message }} </div>
                  @enderror
                </div>
              </div>

              <div class="d-flex justify-content-end">
                <button type="submit" class="btn btn-success"> SIMPAN </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('plugin')
<link rel="stylesheet" href="/assets/adminlte/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

<script src="/assets/adminlte/plugins/select2/js/select2.full.min.js"></script>
@endsection

@section('javascript')
<script type="text/javascript">
  (function ($) {
    $(document).ready(function () {

    })
    $('.select2').select2({
      theme: 'bootstrap4',
      placeholder: 'Silahkan pilih',
    })
    $('#nik').select2({
        allowClear: true,
        placeholder: 'Silahkan ketik nama karyawan',
        minimumInputLength: 1,
        theme: 'bootstrap4',
        ajax: {
            delay: 250,
            dataType: 'JSON',
            method: 'GET',
            url: '/user/search-karyawan',
            cache: false,
            data: (params) => ({ search: params.term}),
            processResults: (result, params) => ({ results: result.data.data.map(e => ({ id: e.nik, text: `${e.nip} | ${e.nama}`})) })
        }, 
    })
    $('#nik').change( e =>{
      let nikValue = $('#nik').val()
      if(nikValue){
        let username = 'K' + nikValue.padStart(4, '0');
        $('#username').val(username)

        let nikText = $( "#nik option:selected" ).text()
        $('#nik_text').val(nikText)
      }else{
        $('#username').val("")
        $('#password').val("")
        $('#nik_text').val("")
      }
    })
  })(jQuery)
</script>
@endsection