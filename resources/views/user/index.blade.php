@extends('layout.app')
@section('title', 'user - index')
@section('breadcumb', 'User')

@section('content')
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Data user</h3>

            <div class="card-tools">
              <a href="{{ route('user.create') }}" class="btn btn-success btn-sm">Tambah data</a>
            </div>
          </div>
          <div class="card-body table-responsive" >
            @if(session('status'))
              <div  class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              @endif
            <table class="table table-sm table-head-fixed text-nowrap" id="datatables">
              <thead>
                <tr>
                  <th>NIK</th>
                  <th>username</th>
                  <th>Nama</th>
                  <th>Akses</th>
                  <th>#</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('plugin')
<link rel="stylesheet" href="/assets/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/assets/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

<script src="/assets/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/assets/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="/assets/adminlte/plugins/moment/moment.min.js"></script>
<script src="/assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/assets/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
@endsection

@section('javascript')
<script type="text/javascript">
  (function ($) {
    $('#datatables').DataTable({
          processing: true,
          serverSide: true,
          stateSave: true,
          ajax: "/user/get-listing",
          bLengthChange: false,
          ordering: false,
          pageLength: 15,
          bFilter: true,
          columns: [
              {
                data: 'nik', 
                name: 'nik',
                orderable: true, 
                searchable: false
              },
              {
                data: 'username', 
                name: 'username',
                orderable: false, 
                searchable: true
              },
              {
                data: 'nama', 
                name: 'nama',
                orderable: false, 
                searchable: true
              },
              {
                data: 'group', 
                name: 'group',
                orderable: false, 
                searchable: false
              },
              {
                data: 'action', 
                name: 'action', 
                orderable: false, 
                searchable: false
              },
          ]
      });
  })(jQuery)
</script>
@endsection