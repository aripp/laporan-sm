<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WebUserUpdateReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id" => "required",
            "nik" => "required",
            "username" => "required",
            "group" => "required",
        ];
    }
    public function messages()
    {
        return [
            'id.required' => 'id harus diisi',
            'username.required' => 'Username harus diisi',
            'nik.required' => 'Karyawan harus diisi',
            'group.required' => 'Group harus diisi',
        ];
    }
}
