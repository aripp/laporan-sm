<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Helpers\Facades\MyHelpers;
use App\Http\Controllers\Controller;
use App\Repositories\LaporanPoliRepo;
use App\Repositories\MasterPoliRepo;

class PoliController extends Controller
{
    
    public function lapDemografi(Request $request, LaporanPoliRepo $lapPoliRepo){
        $startDate = Carbon::create("$request->start_date")->isoFormat('YYYY-MM-DD');
        $endDate = Carbon::create("$request->end_date")->isoFormat('YYYY-MM-DD');
        $poli = $request->poli;

        $data = [
            'jenis_kelamin' => $lapPoliRepo->jenisKelamin($startDate, $endDate, $poli),
            'kunjungan' => $lapPoliRepo->jenisKunjungan($startDate, $endDate, $poli),
            'asuransi' => $lapPoliRepo->asuransi($startDate, $endDate, $poli),
            'agama' => $lapPoliRepo->agama($startDate, $endDate, $poli),
            'pendidikan' => $lapPoliRepo->pendidikan($startDate, $endDate, $poli),
            'total_kujungan' => $lapPoliRepo->countAll($startDate, $endDate, $poli),
        ];

        return MyHelpers::myJsonResponse(200, "Ok", $data);
    }
    
    public function getPoli(MasterPoliRepo $masterPoliRepo)
    {
        return MyHelpers::myJsonResponse(200, "Ok", $masterPoliRepo->listing());
    }

    
    
}
