<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\WebAuthReq;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        return view('auth.index');
    }
    public function authenticate(WebAuthReq $request)
    {
        $username = $request->username;
        $password = $request->password;
        $remember = $request->remember ? true : false;
 
        if (Auth::attempt(['username' => $username, 'password' => $password], $remember)) {
            $request->session()->regenerate();
 
            return redirect()->intended('dashboard');
        }
 
        return back()->with(['status' => 'Username / password salah'])->onlyInput('username');
    }
    public function logout(Request $request)
    {
        return view('auth.index');
    }
}
