<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Repositories\UserRepo;
use Yajra\DataTables\DataTables;
use App\Helpers\Facades\MyHelpers;
use App\Repositories\KaryawanRepo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\WebUserStoreReq;
use App\Http\Requests\WebUserUpdateReq;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(WebUserStoreReq $request, UserRepo $userRepo)
    {
        $user = [
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'nik' => $request->nik,
            'group' => $request->group,
        ];
        
        if($userRepo->isExists($user['nik'], $user['username'])){
            return back()->with(['status' => 'NIK sudah terdaftar'])->withInput();
        }

        $userRepo->save($user);
        return redirect()->route('user.index')->with(['status' => 'Data berhasil ditambahkan']);

    }

    public function listing(Request $request, UserRepo $userRepo)
    {
        
        $data = $userRepo->datatable($request);
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $action =  '<a href="'.route('user.edit', ['id' => $row['id']]).'" class="edit btn btn-warning btn-sm">Edit</a> 
                            <a href="'.route('user.delete', ['id' => $row['id']]).'" class="delete btn btn-danger btn-sm">Delete</a>';
                return $action;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function edit(Request $request, UserRepo $userRepo)
    {
        $id = $request->id;
        $data = $userRepo->getById($id);

        if(!$data){
            return abort(404);
        }

        return view('user.edit', ['data' => $data]);
    }

    public function update(WebUserUpdateReq $request, UserRepo $userRepo)
    {
        $id = $request->id;
        $user = [
            'group' => $request->group,
        ];

        if($request->password){
            $user['password'] = Hash::make($request->password);
        }

        $userRepo->update($id, $user);
        return redirect()->route('user.index')->with(['status' => 'Data berhasil diubah']);

    }
    public function delete(Request $request, UserRepo $userRepo)
    {
        $id = $request->id;
        $deleted = $userRepo->delete($id);

        if($deleted){
            return redirect()->route('user.index')->with(['status' => 'Data berhasil dihapus']);
        }
        
        return redirect()->route('user.index')->with(['status' => 'Data gagal dihapus']);

    }
    public function searchKaryawan(Request $request, KaryawanRepo $karyawanRepo)
    {
        $search = $request->search;
        $data = [
            'data' => $karyawanRepo->getByNama($search)
        ];

        return MyHelpers::myJsonResponse(200, "Ok", $data);
    }
}
