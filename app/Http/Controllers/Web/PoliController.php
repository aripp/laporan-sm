<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Repositories\KunjungRepo;
use App\Helpers\Facades\MyHelpers;
use App\Http\Controllers\Controller;
use App\Repositories\MasterPoliRepo;

class PoliController extends Controller
{
    public function demografi(){
        $data = [
            'title' => 'Poli - Demografi',
            'breadcumb' => 'Demografi'
        ];

        return view('poli.demografi', $data);
    }

    public function getPoli(MasterPoliRepo $masterPoliRepo)
    {
        return MyHelpers::myJsonResponse(200, "Ok", $masterPoliRepo->listing());
    }

}
