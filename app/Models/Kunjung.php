<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kunjung extends Model
{
    protected $connection = 'simrs';
    protected $table = 'kunjung';
    protected $primaryKey = 'kunjung_id';
    public $timestamps = false;

}