<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterAgama extends Model
{
    protected $connection = 'simrs';
    protected $table = 'master_agama';
    protected $primaryKey = 'id_agama';
    public $timestamps = false;
}
