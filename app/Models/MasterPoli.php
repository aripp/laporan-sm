<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPoli extends Model
{
    use HasFactory;
    protected $connection = 'simrs';
    protected $table = 'master_poli';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
