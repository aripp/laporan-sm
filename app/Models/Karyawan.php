<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $connection = 'sdm';
    protected $table = 'karyawan';
    protected $primaryKey = 'nik';
    public $timestamps = false;

}
