<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;

class MyHelpers
{
    
    public function myJsonResponse($resStatusCode, $resMessage, $resData = null)
    {
        $metadata = [
            'message' => $resMessage,
            'status_code' => $resStatusCode,
            'date' => Carbon::now()->getTimestampMs(),
        ];
        $response = [
            'metadata' => $metadata
        ];
        if($resData){
            $response = [
                'metadata' => $metadata,
                'data' => $resData
            ];
        }

        
        return response()->json($response, $resStatusCode);
    }
}