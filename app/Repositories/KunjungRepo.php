<?php
namespace App\Repositories;

use App\Models\Kunjung;

class KunjungRepo{

    private $LIST_POLI_BUKAN_LAPORAN = ['P001', 'P006', 'P007', 'P012'];

    public function getByTglMasuk($tanggal)
    {
        return Kunjung::select([
            'kunjung.kunjung_id',
            'kunjung.sts_inap',
            'pasien.no_rm',
            'pasien.nama',
            'master_poli.poli_nama',
            'master_dokter.dokter_nama'
        ])
        ->join('pasien', 'pasien.no_rm', '=', 'kunjung.no_rm')
        ->join('dokter_jadwal', 'dokter_jadwal.id', '=', 'kunjung.jadwal_id')
        ->join('master_poli', 'master_poli.poli_kd', '=', 'dokter_jadwal.poli_kd')
        ->join('master_dokter', 'master_dokter.dokter_kd', '=', 'dokter_jadwal.dokter_kd')
        ->where('kunjung.tgl_masuk', $tanggal)
        ->where('kunjung.deleted', 0)
        ->get();
    }
    public function lapPoliJenisKelamin($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('pasien.jenis_kelamin, count(kunjung.kunjung_id) as total')
        ->join('pasien', 'pasien.no_rm', '=', 'kunjung.no_rm')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->LIST_POLI_BUKAN_LAPORAN);

        if($poli == "SEMUA"){
            return $query->groupBy('pasien.jenis_kelamin')->get();
        }else{
            return $query->where('kunjung.poli_kd', $poli)->groupBy('pasien.jenis_kelamin')->get();
        }
        
    }
}