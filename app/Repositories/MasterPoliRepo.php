<?php
namespace App\Repositories;

use App\Models\MasterPoli;

class MasterPoliRepo{

    private $BUKAN_POLI = ['P001', 'P006', 'P007', 'P012'];
    
    public function listing()
    {
        return MasterPoli::where('deleted', 0)->whereNotIn('poli_kd', $this->BUKAN_POLI)->get();
    }
}