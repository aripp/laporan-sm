<?php
namespace App\Repositories;

use App\Models\Kunjung;

class LaporanPoliRepo{

    private $BUKAN_POLI = ['P001', 'P006', 'P007', 'P012'];

    public function jenisKelamin($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('pasien.jenis_kelamin, count(kunjung.kunjung_id) as total')
        ->join('pasien', 'pasien.no_rm', '=', 'kunjung.no_rm')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }

        return $query->groupBy('pasien.jenis_kelamin')->get();
        
    }
    public function agama($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('master_agama.agama, count(kunjung.kunjung_id) as total')
        ->join('pasien', 'pasien.no_rm', '=', 'kunjung.no_rm')
        ->join('master_agama', 'pasien.agama', '=', 'master_agama.agama')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }

        return $query->groupBy('master_agama.agama')->get();
        
    }
    public function pendidikan($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('master_pendidikan.pendidikan, count(kunjung.kunjung_id) as total')
        ->join('pasien', 'pasien.no_rm', '=', 'kunjung.no_rm')
        ->join('master_pendidikan', 'pasien.pendidikan', '=', 'master_pendidikan.pendidikan')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }

        return $query->groupBy('master_pendidikan.pendidikan')->get();
        
    }
    public function jenisKunjungan($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('kunjung.jenis_kunjung, count(kunjung.kunjung_id) as total')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }
        
        return $query->groupBy('kunjung.jenis_kunjung')->get();
        
    }
    public function asuransi($startDate, $endDate, $poli)
    {
        $query = Kunjung::selectRaw('master_asuransi.asuransi_group, count(kunjung.kunjung_id) as total')
        ->join('master_asuransi', 'master_asuransi.asuransi_id', '=', 'kunjung.asuransi_id')
        ->whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }
        
        return $query->groupBy('master_asuransi.asuransi_group')->get();
        
    }
    public function countAll($startDate, $endDate, $poli)
    {
        $query = Kunjung::whereBetween('kunjung.tgl_masuk', [$startDate, $endDate])
        ->where('kunjung.deleted', 0)
        ->whereNotIn('kunjung.poli_kd', $this->BUKAN_POLI);

        if($poli != "SEMUA"){
            $query->where('kunjung.poli_kd', $poli);
        }
        
        return $query->count();
        
    }
}