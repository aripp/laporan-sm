<?php
namespace App\Repositories;

use App\Models\User;
use App\Models\Karyawan;

class UserRepo{

    public function isExists($nik, $username)
    {
        return User::where('nik', $nik)->orWhere('username', $username)->exists();
    }

    public function save($data)
    {
        return User::create($data);
    }

    public function update($id, $data)
    {
        return User::where('id', $id)->update($data);
    }

    public function delete($id)
    {
        return User::where('id', $id)->delete();
    }

    public function datatable($request)
    {
        $user = User::latest()->get();
        $data = [];
        foreach($user as $r){
            $data[] = [
                'id' => $r->id,
                'nik' => $r->nik,
                'nama' => Karyawan::where('nik', $r->nik)->value('nama'),
                'group' => $r->group,
                'username' => $r->username,
            ];
        }
        return collect($data);
    }

    public function getById($id)
    {
        return User::where('id', $id)->first();
    }
}