<?php
namespace App\Repositories;

use App\Models\Karyawan;

class KaryawanRepo{

    public function getByNama($nama)
    {
        return Karyawan::select([
            'nik',
            'nip',
            'nama'
        ])
        ->where('deleted', 0)
        ->where('nama','like',"%$nama%")
        ->get();
    }
}