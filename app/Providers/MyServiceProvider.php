<?php

namespace App\Providers;

use App\Helpers\MyHelpers;
use Illuminate\Support\ServiceProvider;

class MyServiceProvider extends ServiceProvider
{
    
    public function register()
    {
        // bind helpers facade
        $this->app->bind('MyHelpers', function ($app) {
            return new MyHelpers();
        });
        
        // bind service jika pakai service
        // logic bisa reuse jika ingin membuat api, console atau website
        // 
        // $this->app->singleton(App\Services\KodingService::class, function ($app) {
        //     return new KodingService(
        //         $app->make(App\Repositories\KunjungRepo::class),
        //     );
        // });
    }

    
    public function boot()
    {
        
    }
}
